PI-Mobile documentation project based on [Jekyll](http://jekyllrb.com)

Generated output can be found [here](https://pi-mobile.gitlab.io/pi-mobile-doc)

## Setup of Jekyll preview on Ubuntu
1. Follow instructions on https://jekyllrb.com/docs/installation/ubuntu/
2. Remove Gemfile.lock
3. Run "bundle install" in terminal
4. Now you can execute "./run.sh" in WebStorm or IntelliJ terminal
